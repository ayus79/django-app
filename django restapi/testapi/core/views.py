from rest_framework.decorators import api_view, APIView, action
from rest_framework.response import Response
from .serializer import ProductSerializer, UserSerializer
from .models import Product
from rest_framework.authtoken.models import Token
from django.contrib.auth.models import User
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
import logging


#for logs -------
logger = logging.getLogger(__name__)

# product rest api classbased view
class ProductView(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request):
        try:
            product_objs = Product.objects.all()
            serializer = ProductSerializer(product_objs, many=True)
            return Response({
                'status': True,
                'message': 'product Fetched',
                'data': serializer.data
            })

        except Exception as e:
            logger.error(f': {e}')
            return Response({
                'status': False,
                'message': 'something went wrong'
            })

    def post(self, request):
        try:
            data = request.data
            serializer = ProductSerializer(data=data)
            if serializer.is_valid():
                serializer.save()
                return Response({
                    'status': True,
                    'message': 'success data',
                    'data': serializer.data
                })
            return Response({
                'status': False,
                'message': 'invalid data',
                'data': serializer.errors
            })

        except Exception as e:
            logger.error(f': {e}')
            return Response({
                'status': False,
                'message': 'something went wrong'
            })

    def patch(self, request):
        try:
            data = request.data
            if not data.get('id'):
                return Response({
                'status': False,
                'message': 'id is required',
                'data': {}
            })

            obj = Product.objects.get(id= data.get('id'))
            serializer = ProductSerializer(obj, data=data, partial=True)
            if serializer.is_valid():
                serializer.save()
                return Response({
                    'status': True,
                    'message': 'success id',
                    'data': serializer.data
                })
            return Response({
                'status': False,
                'message': 'invalid id',
                'data': serializer.errors
            })

        except Exception as e:
            logger.error(f': {e}')
            return Response({
                'status': False,
                'message': 'something went wrong'
            })

    def put(self, request):
        try:
            data = request.data
            if not data.get('id'):
                return Response({
                'status': False,
                'message': 'id is required',
                'data': {}
            })

            obj = Product.objects.get(id= data.get('id'))
            serializer = ProductSerializer(obj, data=data)
            if serializer.is_valid():
                serializer.save()
                return Response({
                    'status': True,
                    'message': 'success id',
                    'data': serializer.data
                })
            return Response({
                'status': False,
                'message': 'invalid id',
                'data': serializer.errors
            })

        except Exception as e:
            logger.error(f': {e}')
            return Response({
                'status': False,
                'message': 'something went wrong'
            })

    def delete(self, request):
        try:
            data = request.data
            if not data.get('id'):
                return Response({
                'status': False,
                'message': 'id is required',
                'data': {}
            })

            obj = Product.objects.get(id= data.get('id'))
            obj.delete()
            return Response({
                'status': True,
                'message': 'success delete',
                'data': {}
            })

        except Exception as e:
            logger.error(f': {e}')
            return Response({
                'status': False,
                'message': 'something went wrong'
            })

# user register classbased view
class  RegisterUser(APIView):
    
    def post(self, request):
        try:
            serializer = UserSerializer(data= request.data)
            if not serializer.is_valid():
                return Response({
                    'status': False,
                    'message': 'serializer not valid',
                    'data': serializer.errors
                })

            serializer.save()
            user = User.objects.get(username= serializer.data['username'])
            token_obj , _= Token.objects.get_or_create(user = user)
            return Response({
                'status': True,
                'message': 'user serializer working',
                'token': str(token_obj),
                'data': serializer.data
            })
        except Exception as e:
            logger.error(f': {e}')
            return Response({
                'status': False,
                'message': 'something went wrong'
            })
