from django.contrib import admin
from .models import Product

# Register models
admin.site.register(Product)