from django.urls import path
from core import views


urlpatterns = [
    path('product/', views.ProductView.as_view()),
    path('register/', views.RegisterUser.as_view()),
]   
