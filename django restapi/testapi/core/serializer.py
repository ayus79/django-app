from rest_framework import serializers
from django.contrib.auth.models import User
from .models import Product
import re


# user serializer
class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ['username','password']

    def create(self, validated_data):
        user = User.objects.create(username =validated_data['username'])
        user.set_password(validated_data['password'])
        user.save()
        return user


# product serializer
class ProductSerializer(serializers.ModelSerializer):

    class Meta:
        model = Product
        fields = ['product_title', 'product_description', 'price']
        # exclude = ['uid', 'created_at', 'updated_at']

    def validate_product_title(self, data):
        if data:
            product_title = data
            regex = re.compile("[!@#$%^&*()-+?_=,<>/\}{~]")
            if len(product_title) < 3:
                raise serializers.ValidationError(
                    'Product title must be 3 characters or morethen that')
            if not regex.search(product_title) == None:
                raise serializers.ValidationError(
                    'Product title cannot contains special characters')
        return data

    def validate_product_description(self, data):
        if data:
            product_description = data
            regex = re.compile("[!@#$%^&*()-+?_=,<>/\}{~]")
            if not regex.search(product_description) == None:
                raise serializers.ValidationError(
                    'Product description cannot contains special characters')
        return data
 
    def validate_price(self, data):
        if data:
            price = data
            if price >= 400:
                raise serializers.ValidationError(
                    'price cannot be set morethan specified amount')
        return data
