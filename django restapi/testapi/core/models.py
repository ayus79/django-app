from django.db import models

# product model for RESTAPI
class Product(models.Model):
    product_title = models.CharField(max_length=100)
    product_description = models.TextField()
    price = models.IntegerField()

    def __str__(self):
        return self.product_title
