from __future__ import absolute_import,unicode_literals
import os
from celery import Celery
from django.conf import settings
from celery.schedules import crontab


#celery configuration
os.environ.setdefault('DJANGO_SETTINGS_MODULE','testapi.settings')

app = Celery('testapi')
app.conf.enable_utc = False
app.conf.update(timezone= 'Asia/Kolkata')
app.config_from_object(settings, namespace='CELERY')
app.autodiscover_tasks()
app.conf.beat_schedule = {
    'every-1-miute-delete-tokens':{
        'task': 'productapp.tasks.delete_token',
        'schedule': 60,
    }
}

@app.task(bind=True)
def debug_task(self):
    print(f'Request: {self.request!r}')
