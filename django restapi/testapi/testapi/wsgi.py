# import os
# from django.core.wsgi import get_wsgi_application
# from waitress import serve

# os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'testapi.settings')

# application = get_wsgi_application()

# serve(application)
#waitress setup for wsgi server
import os
from django.core.wsgi import get_wsgi_application
from waitress import serve
from threading import Thread

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'testapi.settings')

application = get_wsgi_application()

def run_waitress():
    serve(application, host='localhost', port=8000)

Thread(target=run_waitress).start()
