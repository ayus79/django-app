# import os
# from django.core.asgi import get_asgi_application
# from uvicorn import run

# os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'testapi.settings')

# application = get_asgi_application()

# run(application)

#uvicorn setup for asgi server
import os
from django.core.asgi import get_asgi_application
from uvicorn import run
from threading import Thread

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'testapi.settings')

application = get_asgi_application()

def run_uvircorn():
    run(application, host='localhost', port=8080)

Thread(target=run_uvircorn).start()
