from django.shortcuts import render, redirect, HttpResponse,HttpResponseRedirect
from django.contrib.auth.models import User
from productapp.models import Profile,Product
from django.contrib import messages
from django.contrib.auth import authenticate,login,logout
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator
from .email_sender import send_forget_pass_mail,send_registration_mail
from django.contrib.auth import update_session_auth_hash
import uuid
from .tasks import sleepy,pass_reset_mail
import time
from django.utils import timezone
import csv
import datetime
import xlwt
import threading
import logging


#for logs -------
logger = logging.getLogger(__name__)

# register user
def registerpage(request):
    try:
        if request.method == "POST":
            username = request.POST.get('username')
            email = request.POST.get('email')
            password = request.POST.get('password')

            if User.objects.filter(username=username).first() is not None:
                messages.warning(request, 'Sorry, Username already exist.')
                return redirect('register')
            if User.objects.filter(email=email).first() is not None:
                messages.warning(request, 'Sorry, Email already exist.')
                return redirect('register')

            myuser = User.objects.create_user(username, email, password)
            try:
                myuser.save()
                messages.success(request, 'Account created successfully.')
                # send email
                if email:
                    mail_subject = f'{username} your account created successfully'
                    message = f'{username} welcome to our app:)'
                    send_registration_mail(mail_subject,message,email)
                    messages.info(request, 'Registration mail sent')
                sleepy(1)
                return redirect('login')
            except Exception as e:
                logger.error(f': {e}')
                messages.error(request, 'Registration failed, try again')
                return redirect('register')
    except Exception as e:
        logger.error(f': {e}')
        messages.error(request, 'Something went wrong')

    return render(request, 'register.html')

# login to account
def loginpage(request):
    try:
        if request.method == "POST":
            username = request.POST.get('username')
            password = request.POST.get('password')
            user = authenticate(request, username =username, password = password)
            if user is not None:
                login(request,user)
                return redirect('home')
            else:
                messages.error(request, 'Wrong username or password')
                return redirect('login')
    except Exception as e:
        logger.error(f': {e}')
        messages.error(request, 'Something went wrong')

    return render(request, 'login.html')
    
# logout to account
def logoutpage(request):
    try:
        logout(request)
        sleepy(1)
        return redirect('login')
    except Exception as e:
        logger.error(f': {e}')
        messages.error(request, 'Something went wrong')

# forget password
def forgetpasswordpage(request):
    try:
        if request.method == "POST":
            username = request.POST.get('username')
            if not User.objects.filter(username=username).first():
                messages.error(request, 'Username not found.')
                return redirect('forgetpassword')

            user_obj = User.objects.get(username=username)
            token = str(uuid.uuid4())
            profile_user = Profile.objects.filter(user= user_obj)
            if profile_user:
                profile_user.delete()
            profile_obj = Profile.objects.create(user= user_obj)
            profile_obj.forget_password_token = token
            profile_obj.save()
            send_forget_pass_mail(user_obj.email, token)
            if send_forget_pass_mail:
                messages.info(request, 'Reset password link has been sent.')
            return redirect('forgetpassword')
    except Exception as e:
        logger.error(f': {e}')
        messages.error(request, 'Something went wrong')

    return render(request, 'forgetpass.html')

# change password without login
def changepasswordpage(request, token):
    try:
        context = {}
        profile_obj = Profile.objects.filter(forget_password_token= token).first()
        context = {'user_id': profile_obj.user.id}
        if request.method == "POST":
            new_password = request.POST.get('password')
            confirm_password = request.POST.get('password1')
            user_id = request.POST.get('user_id')

            if user_id is None:
                messages.error(request, 'User ID not found')
                return redirect(f'/change-password/{token}/')
            if new_password != confirm_password:
                messages.error(request, 
                            'New password and confirm password not matched')
                return redirect(f'/change-password/{token}/')

            user_obj = User.objects.get(id= user_id)
            user_obj.set_password(new_password)
            user_obj.save()
            messages.success(request, 'Password changed')
            return redirect('login')
    except Exception as e:
        logger.error(f': {e}')
        messages.error(request, 'Something went wrong')

    return render(request, 'changepass.html', context)

# reset password with login
def resetpasswordpage(request):
    try:
        product_data = Product.objects.all()
        context = {
            'product_data': product_data,
        }
        if request.method == "POST":
            new_password = request.POST.get('password')
            confirm_password = request.POST.get('password1')

            if new_password != confirm_password:
                messages.error(request, 'New password and confirm password not matched')
                return redirect('resetpassword')

            user_obj = User.objects.get(id = request.user.id)
            user_obj.set_password(new_password)
            user_obj.save()
            sendmail_passchange(request)
            update_session_auth_hash(request, user_obj)
            messages.success(request, 'password changed')
            return redirect('home')
    except Exception as e:
        logger.error(f': {e}')
        messages.error(request, 'Something went wrong')

    return render(request, 'changepass.html')

#send mail on password changed auth user only
def sendmail_passchange(request):
    try:
        new_password = request.user.password
        old_password = User.objects.get(id=request.user.id).password
        if old_password != new_password:
            to_mail = request.user.email
            pass_reset_mail(to_mail)
    except Exception as e:
        logger.error(f': {e}')
        messages.error(request, 'Something went wrong')

# home page (product page)
@login_required(login_url='/login/')
def homepage(request):
    try:
        user_data = User.objects.all()
        product_data = Product.objects.all()
        # pagination
        product_paginator = Paginator(product_data, 15)
        page_num = request.GET.get('page')
        page = product_paginator.get_page(page_num)

        context = {
            'data': user_data,
            'product_data': product_data,
            'page': page
        }
    except Exception as e:
        logger.error(f': {e}')
        messages.error(request, 'Something went wrong')

    return render(request, 'index.html', context)

# delete products using checkbox
def delete_product(request):
    try:
        if request.method=="POST":
            product_ids = request.POST.getlist('id[]')
            for id in product_ids:
                product = Product.objects.get(id=id)
                if product:
                    product.delete()
                messages.info(request,'Product deleted')
            return redirect('home')
    except Exception as e:
        logger.error(f': {e}')
        messages.error(request,'Something went wrong.')

    return redirect('home')

# dynamic url for each product
def productpage(request,id):
    try:
        product_data = Product.objects.filter(id=id)
        context = {
            'product_data': product_data
        }
    except Exception as e:
        logger.error(f': {e}')
        messages.error(request,'Something went wrong.')

    return render(request, 'product.html', context)

# -------------------------------- functions --------------------------------------------
#for loading csv data into model
def loadcsv(request):
    try:
        # time1 = time.perf_counter()
        #for deleting previous data
        product_delete = Product.objects.all()
        product_delete.delete()
        with open('products.csv', mode='r') as file:
            csvFile = csv.reader(file)
            data = []
            thread_pool = []
            for lines in csvFile:
                t = threading.Thread(target=create_product, args=(lines, data))
                thread_pool.append(t)
                t.start()
            for t in thread_pool:
                t.join()
            Product.objects.bulk_create(data)
        messages.success(request, 'CSV Imported')
        # time2 = time.perf_counter()
        # print('time taken: ',time2-time1)
    except Exception as e:
        logger.error(f': {e}')
        messages.error(request,'Something went wrong.')
    return redirect('home')

# for threading----^-----
def create_product(lines, data):
    created = Product(
            handle=lines[0],
            title=lines[1],
            body=lines[2],
            vendor=lines[3],
            type=lines[4],
            tags=lines[5],
            published=lines[6],
            variant_sku=lines[13],
            variant_inventory_tracker=lines[15],
            variant_price=lines[18],
            image_src=lines[23]
        )
    data.append(created)

#for exporting excel data from model
def export_excel(request):
    try:
        response = HttpResponse(content_type='application/ms-excel')
        response['Content-Disposition'] = 'attachment; filename=products-' + \
        str(datetime.datetime.now())+ '.xls'
        wb = xlwt.Workbook(encoding='utf-8')
        ws = wb.add_sheet('Products')
        row = 0
        font_style = xlwt.XFStyle()
        font_style.font.bold = True

        columns = ['Handle','Title','Body','Vendor','Type','Tags','Published','Variant_sku','Variant_inventory_tracker','Variant_price','Image_src']
        for col in range(len(columns)):
            ws.write(row, col, columns[col], font_style)

        font_style = xlwt.XFStyle()
        rows = Product.objects.all().values_list('handle','title','body','vendor','type','tags','published','variant_sku','variant_inventory_tracker','image_src')
        for row_loop in rows:
            row += 1
            for col in range(len(row_loop)):
                ws.write(row, col, str(row_loop[col]), font_style)
        wb.save(response)
        return response
    except Exception as e:
        logger.error(f': {e}')
        messages.error(request,'Something went wrong.')
    return redirect('home')