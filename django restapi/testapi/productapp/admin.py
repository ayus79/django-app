from django.contrib import admin
from productapp.models import Profile,Product

# registered models
admin.site.register(Profile)
admin.site.register(Product)