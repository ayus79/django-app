from django.db import models
from django.contrib.auth.models import User
from django.utils.text import slugify

#user auth token model
class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    forget_password_token = models.CharField(max_length=100)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.user.username


#product model
class Product(models.Model):

    class Meta:
        ordering = ['id']

    handle = models.CharField(max_length=300,null=True,blank=True)
    title = models.CharField(max_length=300,null=True,blank=True)
    body = models.TextField(null=True,blank=True)
    vendor = models.CharField(max_length=400,null=True,blank=True)
    type = models.CharField(max_length=300,null=True,blank=True)
    tags = models.CharField(max_length=300,null=True,blank=True)
    published = models.CharField(max_length=200,null=True,blank=True)
    variant_sku = models.CharField(max_length=200,null=True,blank=True)
    variant_inventory_tracker = models.CharField(max_length=200,
                                                 null=True,blank=True)
    variant_price = models.CharField(max_length=200,null=True,blank=True)
    image_src = models.URLField(max_length=200,null=True,blank=True)

    def __str__(self):
        return self.handle
