from django.core.mail import send_mail
from django.conf import settings
from django.contrib import messages
import logging


#for logs -------
logger = logging.getLogger(__name__)

#forget password email
def send_forget_pass_mail(email, token):
    try:
        subject = 'Forget Password Link'
        message = f'Hello, Click on the link to reset password http://localhost:8000/change-password/{token}/'
        email_from = settings.EMAIL_HOST_USER
        recipient_list = [email]
        send_mail(subject,message,email_from,recipient_list)
        return True
    except Exception as e:
        logger.error(f': {e}')
        messages.error('Something went wrong')

#registration email
def send_registration_mail(subject,message,email):
    try:
        email_from = settings.EMAIL_HOST_USER
        recipient_list = [email]
        send_mail(subject,message,email_from,recipient_list)
        return True
    except Exception as e:
        logger.error(f': {e}')
        messages.error('Something went wrong')
