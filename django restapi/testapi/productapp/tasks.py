from celery import shared_task
from time import sleep
from django.core.mail import send_mail
from django.conf import settings
from productapp.models import Profile

#celery task for  sleep for given seconds
@shared_task(bind=True)
def sleepy(self,seconds):
    sleep(seconds)

#celery task for password changed mail
@shared_task(bind=True)
def pass_reset_mail(self,to_mail):
    subject = 'Your password changed'
    message = '''Dear,Your password has been changed recently. 
    If it was not you then contact our customer support.'''
    email_from = settings.EMAIL_HOST_USER
    recipient_list = [to_mail]
    send_mail(subject,message,email_from,recipient_list)
    return True
