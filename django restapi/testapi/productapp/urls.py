from django.urls import path
from productapp import views

#all url patterns
urlpatterns = [
    path('register/', views.registerpage, name='register'),
    path('login/', views.loginpage, name='login'),
    path('home/logout/', views.logoutpage, name='logout'),
    path('forget-password/', views.forgetpasswordpage, name='forgetpassword'),
    path('change-password/<token>/', views.changepasswordpage, 
         name='changepassword'),
    path('home/reset-password/', views.resetpasswordpage, 
         name='resetpassword'),
    path('home/loadcsv/', views.loadcsv, name='csvfile'),
    path('home/export-excel/', views.export_excel, name='exportexcel'),
    path('home/', views.homepage, name='home'),
    path('home/product/<str:id>', views.productpage, name='product'),
    path('home/delete-product/', views.delete_product, name='deleteproduct'),
]
