# Generated by Django 4.1.7 on 2023-04-03 05:22

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('productapp', '0008_product_is_delete'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='product',
            name='is_delete',
        ),
    ]
